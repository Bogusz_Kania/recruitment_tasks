# README #

Python scripts are name accordingly to tasks.  File “test_blast.b6” is used for testing “tsv_proc.py”.

# COMMENTS #

Please review example output for FASTQ processing in tasks description.
Regarding PHRED quality description from: https://biopython.org/docs/1.75/api/Bio.SeqIO.QualityIO.html  
this exemplary output seems to be incorrect.

Sample input:  
@read.1  
GTTTGGGGAT  
+  
BBBFFFFFGH  
@read.2  
GCCTAGTGGC  
+  
CCCFFFDFHH  
@read.3  
GTCAGCGTTT  
+  
@CCFFDFBHH  
Sample output:  
read_position mean_Phred_qual standard_deviation_Phred_qual  
1 32.6667 1.5275  
2 33.6667 0.5774  
3 33.6667 0.5774  


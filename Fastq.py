import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# all conventions regarding FASTQ format taken from:
# https://biopython.org/docs/1.75/api/Bio.SeqIO.QualityIO.html
# https://doi.org/10.1093/nar/gkp1137


class SimpleFastqQualityParser:
    file_path = None
    sequence_identifiers = []
    sequence_qualities = []
    quality_means = []
    quality_stddevs = []

    def __init__(self, file_path):
        self.file_path = file_path

    @staticmethod
    def decode_quality(qual_string):
        """ Returns value of quality from string.
        Reference results below are based on output from Bio.SeqIO.parse.

        >>> SimpleFastqQualityParser.decode_quality("BBBFFFFFGHHHHJJJJJJJJJIJJJJJJJJJJJJJGIJJJJJJIIJJJFFHIJHHHHHHHFFFFFEEEEEEDCDCEDDDDDDDDDDDDDDD")
        [33, 33, 33, 37, 37, 37, 37, 37, 38, 39, 39, 39, 39, 41, 41, 41, 41, 41, 41, 41, 41, 41, 40, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 38, 40, 41, 41, 41, 41, 41, 41, 40, 40, 41, 41, 41, 37, 37, 39, 40, 41, 39, 39, 39, 39, 39, 39, 39, 37, 37, 37, 37, 37, 36, 36, 36, 36, 36, 36, 35, 34, 35, 34, 36, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35]
        >>> SimpleFastqQualityParser.decode_quality("CCCFFFFFHGHGHJJIIJJJJJJIJJGIIIJJIJJJJGJJJJJIJJJJIIHIEGHHIIHIJJIJJIIJJJJJHHHHFFFFFFFDEEEEBDDD")
        [34, 34, 34, 37, 37, 37, 37, 37, 39, 38, 39, 38, 39, 41, 41, 40, 40, 41, 41, 41, 41, 41, 41, 40, 41, 41, 38, 40, 40, 40, 41, 41, 40, 41, 41, 41, 41, 38, 41, 41, 41, 41, 41, 40, 41, 41, 41, 41, 40, 40, 39, 40, 36, 38, 39, 39, 40, 40, 39, 40, 41, 41, 40, 41, 41, 40, 40, 41, 41, 41, 41, 41, 39, 39, 39, 39, 37, 37, 37, 37, 37, 37, 37, 35, 36, 36, 36, 36, 33, 35, 35, 35]
        """
        res = []
        for char in qual_string:
            val = ord(char)-33
            if val < 0 or val > 93:
                raise ValueError("wrong character in quality string")
            res.append(val)
        return res

    def read_fastq(self):
        with open(self.file_path, 'r') as inp_file:
            while True:
                rec = [inp_file.readline() for ctr in range(4)]
                if not all(rec):
                    break
                self.process_record(rec)

    def process_record(self, record):
        record = [r_field.rstrip("\r\n") for r_field in record]
        if len(record[1]) != len(record[3]):
            raise ValueError("raw sequence and quality string length mismatch in " + record[0])
        if record[2][0] != "+":
            raise ValueError("missing expected quality \"+\" token in " + record[0])
        try:
            qual = self.decode_quality(record[3])
        except ValueError:
            raise ValueError("wrong character in quality string in " + record[0])

        self.sequence_identifiers.append(record[0])
        self.sequence_qualities.append(qual)
        self.quality_means.append(np.mean(qual))
        self.quality_stddevs.append(np.std(qual))

    def save_graph(self):
        if self.quality_means is None:
            print("nothing to plot")
            return

        read_position = np.arange(1, len(self.quality_means)+1)
        fig, (mean_plot, stddev_plot) = plt.subplots(2, 1, sharex="all",  figsize=(12, 6))
        fig.suptitle("Read sequences mean quality and standard deviation", fontsize=15)
        plt.subplots_adjust(hspace=0.0, left=0.07, right=0.95)
        mean_plot.grid(True, ls="--", alpha=1.5)
        stddev_plot.grid(True, ls="--", alpha=1.5)
        mean_plot.set_ylabel("mean", fontsize=13)
        stddev_plot.set_ylabel("standard deviation", fontsize=13)
        stddev_plot.set_xlabel("read position", fontsize=13)
        mean_plot.tick_params(axis='y', which='major', labelsize=12)
        stddev_plot.tick_params(axis='both', which='major', labelsize=12)

        mean_plot.scatter(read_position, self.quality_means, color="tab:blue", marker="+", s=18, alpha=0.7)
        stddev_plot.scatter(read_position, self.quality_stddevs, color="tab:orange", marker="+", s=18, alpha=0.7)
        out_file_name = str(self.file_path).split(".")[0] + "_qual-graph.pdf"
        plt.savefig(out_file_name, dpi=300)

    def save_tsv(self):
        if self.quality_means is None:
            print("nothing to plot")
            return

        dct = {"read_position": np.arange(1, len(self.quality_means)+ 1 ),
               "mean_Phred_qual": self.quality_means, "standard_deviation_Phred_qual": self.quality_stddevs}
        df = pd.DataFrame(dct)
        out_file_name = str(self.file_path).split(".")[0] + "_qual-output.tsv"
        df.to_csv(out_file_name, sep="\t", float_format='%.4f', index=False)

    def solve_task(self):
        self.read_fastq()
        self.save_tsv()
        self.save_graph()


if __name__ == '__main__':
    import doctest
    doctest.testmod()

# ======================================================================================================================
#SimpleFastqQualityParser("reads.fastq").solve_task()



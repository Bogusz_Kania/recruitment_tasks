
inp_n = int(input())
inp_m = int(input())

for c in range(inp_n, inp_m+1):
    if c % 3 == 0 and c % 5 == 0:
        print("FizzBuzz")
    elif c % 3 == 0:
        print("Fizz")
    elif c % 5 == 0:
        print("Buzz")
    elif True:
        print(c)

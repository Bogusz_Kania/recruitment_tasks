import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from collections import Counter
from io import StringIO

def process_tsv(in_path, test_data_path=None):
    """ Solves 4th recruitment task.

    >>> process_tsv(in_path="dummy_path", test_data_path="test_blast.b6")
    array([[52,  1],
           [92,  2]])
    """
    if test_data_path is not None:
        in_path = test_data_path
    df = pd.read_csv(in_path, sep="\t", usecols=[0, 3, 10], names=["cquery", "alignment_length", "e_value"])

    length_values = []
    for query in df.cquery.unique():
        # the smaller the e_value, the better the match
        current_min_e_value = df[(df.cquery == query)].e_value.min()
        # by "<=" in "df.e_value <= current_min_e_value" it is allowed to gather multiple alignment_lengths
        # from queries, as long as match quality for that lengths is equal
        selected_lengths = df[(df.cquery == query) & (df.e_value <= current_min_e_value)].alignment_length
        length_values += list(selected_lengths)

    # unique_length_values = list(dict(length_values))
    histogram = Counter(length_values)
    res = np.array([[length, histogram[length]] for length in histogram])
    res.sort(axis=0)

    fig, ax = plt.subplots(1, 1,  figsize=(8, 5))
    fig.suptitle("Abundance of alignment length in BLAST6 file", fontsize=15)
    # plotting standard histogram: length-wise results are available in output CSV
    ax.hist(length_values, edgecolor="white")
    ax.set_ylabel("abundance", fontsize=13)
    ax.set_xlabel("alignment length", fontsize=13)
    ax.tick_params(axis='y', which='major', labelsize=13)
    out_pdf_file_name = str(in_path).split(".")[0] + "_length-abundance-histogram.pdf"
    plt.savefig(out_pdf_file_name, dpi=300)
    out_csv_file_name = str(in_path).split(".")[0] + "_length-abundance-data.csv"
    np.savetxt(out_csv_file_name, res, header="alignment_length,abundance", comments="", delimiter=",", fmt="%i")

    if test_data_path is not None:
        return res


if __name__ == '__main__':
    import doctest
    doctest.testmod()

# ======================================================================================================================
# process_tsv("test_blast.b6")

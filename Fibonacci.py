
inp_n = int(input())
inp_m = int(input())


def fib_from_n_to_m(n, m):
    fib = [0, 1]
    k = m + 1
    # following loop time complexity is O(m)
    # (best achievable time complexity is O(log(m)) with use of the matrix multiplication,
    #  but it would be not my own solution)
    for i in range(2, k):
        fib.append(fib[i-2] + fib[i-1])
    return fib[n:k]


for f_number in fib_from_n_to_m(inp_n, inp_m):
    print(f_number)
